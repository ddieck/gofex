package main

import (
//    "github.com/mattn/go-gtk/gdkpixbuf"
    "github.com/mattn/go-gtk/glib"
    "github.com/mattn/go-gtk/gtk"
    "fmt"
    "strconv"
    "encoding/json"
    "os"
    "github.com/mitchellh/go-homedir"
)

type Account struct {
	Host		string    `json:"Host"`
	Port		int	  `json:"Port"`
	Email		string	  `json:"Email"`
	Authid		string	  `json:"Authid"`
	Passwd		string	  `json:"Passwd"`
	ProxyHost	string	  `json:"ProxyHost"`
	ProxyPort	int	  `json:"ProxyPort"`
}

func main() {
    config := load_config()
    fmt.Println(config.Host)
    gtk.Init(nil)
    window := gtk.NewWindow(gtk.WINDOW_TOPLEVEL)
    window.SetPosition(gtk.WIN_POS_CENTER)
    window.SetTitle("GTK Go!")
    window.SetIconName("gtk-dialog-info")
    window.Connect("destroy", func(ctx *glib.CallbackContext) {
        println("got destroy!", ctx.Data().(string))
        gtk.MainQuit()
    }, "foo")
    page_preferences := gtk.NewFrame("Preferences")
    page_send := gtk.NewFrame("Send")
    page_get := gtk.NewFrame("Get")
    notebook := gtk.NewNotebook()
    notebook.AppendPage(page_preferences, gtk.NewLabel("Preferences"))
    notebook.AppendPage(page_send, gtk.NewLabel("Send"))
    notebook.AppendPage(page_get, gtk.NewLabel("Get"))

    grid_preferences := gtk.NewTable(2,3,false)

    grid_preferences.Attach(gtk.NewLabel("Host"), 0,1,0,1,gtk.FILL,gtk.FILL,2,3)
    host_entry := gtk.NewEntry()
    host_entry.SetText(config.Host)
    grid_preferences.Attach(host_entry, 1,2,0,1,gtk.FILL,gtk.FILL,2,3)

    port_entry := gtk.NewEntry()
    port_entry.SetText(strconv.Itoa(config.Port))
    grid_preferences.Attach(gtk.NewLabel("Port"), 0,1,1,2, gtk.FILL, gtk.FILL, 2,3)
    grid_preferences.Attach(port_entry, 1,2,1,2, gtk.FILL, gtk.FILL, 2,3)

    grid_preferences.Attach(gtk.NewLabel("Sender E-Mail"), 0,1,2,3, gtk.FILL, gtk.FILL, 2,3)
    email_entry := gtk.NewEntry()
    email_entry.SetText(config.Email)
    grid_preferences.Attach(email_entry, 1,2,2,3, gtk.FILL, gtk.FILL, 2,3)

    grid_preferences.Attach(gtk.NewLabel("AuthID"), 0,1,3,4, gtk.FILL, gtk.FILL, 2,3)
    authid_entry := gtk.NewEntry()
    authid_entry.SetText(config.Authid)
    grid_preferences.Attach(authid_entry, 1,2,3,4, gtk.FILL, gtk.FILL, 2,3)

    grid_preferences.Attach(gtk.NewLabel("Password"), 0,1,4,5, gtk.FILL, gtk.FILL, 2,3)
    password_entry := gtk.NewEntry()
    password_entry.SetVisibility(false)
    password_entry.SetText(config.Passwd)

    grid_preferences.Attach(password_entry, 1,2,4,5, gtk.FILL, gtk.FILL, 2,3)
    grid_preferences.Attach(gtk.NewHSeparator(), 0,1,5,6, gtk.FILL, gtk.FILL, 2,3)
    grid_preferences.Attach(gtk.NewLabel("Proxy host"), 0,1,6,7, gtk.FILL, gtk.FILL, 2,3)

    proxy_host_entry := gtk.NewEntry()
    proxy_host_entry.SetText(config.ProxyHost)
    grid_preferences.Attach(proxy_host_entry, 1,2,6,7, gtk.FILL, gtk.FILL, 2,3)
    grid_preferences.Attach(gtk.NewLabel("Proxy port"), 0,1,7,8, gtk.FILL, gtk.FILL, 2,3)

    proxy_port_entry := gtk.NewEntry()
    proxy_port_entry.SetText(strconv.Itoa(config.ProxyPort))
    grid_preferences.Attach(proxy_port_entry, 1,2,7,8, gtk.FILL, gtk.FILL, 2,3)

    button_save := gtk.NewButtonWithLabel("Save")
    button_save.Connect("clicked", func() {
	host := host_entry.GetText()
	port, err := strconv.Atoi(port_entry.GetText())
	if err != nil {
	    fmt.Println(err)
	}
	email := email_entry.GetText()
	authid := authid_entry.GetText()
	password := password_entry.GetText()
	proxy_host := proxy_host_entry.GetText()
	proxy_port, err := strconv.Atoi(proxy_port_entry.GetText())
	if err != nil {
	     fmt.Println(err)
	}

	hash := &Account {
		Host: host,
		Port: port,
		Email: email,
		Authid: authid,
		Passwd: password,
		ProxyHost: proxy_host,
		ProxyPort: proxy_port,
	}

	    fmt.Printf("foo %+v", hash)
	    json,err := json.Marshal(hash)
	    if err != nil {
	        fmt.Println(err)
	    }
	    homedir,err := homedir.Dir()
	    f,err := os.Create(homedir + "/.fexrc")
	    if err != nil {
		fmt.Println(err)
	    }
	    defer f.Close()

	    _, err = f.WriteString(string(json))
	    if err != nil {
		fmt.Println(err)
	    }
	    f.Sync()
        })
    grid_preferences.Attach(button_save, 0,1,8,9, gtk.FILL, gtk.FILL, 2,3)

    page_preferences.Add(grid_preferences)
    window.Add(notebook)
    window.ShowAll()
    gtk.Main()
}

func load_config() Account{
	var config Account
	home, err := homedir.Dir()
	if err != nil {
	   fmt.Println(err)
	}
	data, err := os.Open(home + "/.fexrc")
	if err != nil {
	    fmt.Println(err)
	}
	jsonParser := json.NewDecoder(data)
	err = jsonParser.Decode(&config)
	if err != nil {
	    fmt.Println(err)
	}
	return config
}
